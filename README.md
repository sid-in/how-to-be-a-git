# How to be a Git

3 `stages` for any file. A file can be:  
-  untracked or tracked but not added to staging area  
-  in staging area (also called index)  
-  in repo  

### git add
Add to index
```
git add <filename>
```
Add all tracked files to index
```
git add -u
```
Add all files in current directory to index
```
git add .
```
Add all files to index
```
git add *
```

### git commit

Commit changes (in the index) to repo
Requires a commit message

```
git commit -m "Commit message"
```

For multiline commit messages:
```
git commit
```
The above opens an editor where multiline commit messages can be written.

After a commit HEAD points to the changes that were just committed.

### Local branch v/s remote branch
Any `git commit` commits the change to local branch
To push the changes to a remote branch, use `git push`

### git push

To push a local branch to remote, effectively creating a new remote branch
```
git push -u origin my_branch_name
```
The `-u` flag makes the local branch `my_branch_name` to track the newly created remote branch `remote/my_branch_name` so that the next time you can just use `git push` to push your committed changes to remote.
```
git push
```

### git checkout

Create a new branch from the current branch
```
git checkout -b my_new_branch
```

Switch to an existing branch named `existing_branch`
```
git checkout existing_branch
```

Replace your local changes with the content from HEAD (last commit)
```
git checkout -- filename
```

### git branch

To list all local branches
```
git branch
```

To list all local and remote branches with information about tracking
```
git branch -a
```

To delete a local branch `branch_to_be_deleted`, first switch to another branch
```
git branch -d `branch_to_be_deleted`
```

### git pull

To fetch latest changes from a tracking remote branch
```
git pull
```

If the local and remote branch each have commits that are not synced, i.e. you have commits locally and the remote branch has some commits that you hadn't pulled before making those commits locally
```
git pull --rebase
```
The above command rewinds your local branch commits, pulls the remote branch and applies your local commits on top of the latest remote branch.

### git merge

To merge changes from existing local branch B1 to current branch
```
git merge B1
```
This will copy all commits from B1 to your current branch and may create a new merge commit. If you don't want all those commits to appear on your current branch
```
git merge --squash B1
```
The above command will stop just before committing so you are free to create just one commit that has all the commits from B1
```
git commit
```

### git reset

To unstage a file or undo your `git add filename`
```
git reset HEAD filename
```

To undo 1 commit and put the changes from the commit in the index
```
git reset --soft HEAD~1
```

To undo 1 commit and discard changes from the commit
```
git reset --hard HEAD~1
```

To undo N commits and put the changes from the commit in the index
```
git reset --soft HEAD~N
```

To undo N commit and discard changes from the commit
```
git reset --hard HEAD~N
```

Drop all changes AND **commits** and make your local branch look exactly like remote branch `origin/B1`
```
git fetch origin
git reset --hard origin/B1
```

### git diff

To see the diff between the committed changes in current branch and your local **unstaged** changes
```
git diff
```

To see diff between the committed changes in current branch and index changes (staged changes)
```
git diff --cached
```

To see diff between branches B1 and  B2

```
git diff B1 B2
```
---
## My workflow

Assume that the main branch is `mainline`

\1. Get latest `mainline`
```
git pull
```

\2. Branch off
```
git checkout -b my_awesome_feature
```

\3. Make changes and add to index
```
git add -u
```
If new files are created,
```
git add new_filename
```

\4.  Commit and push
```
git commit -m "some commit message"
git push -u origin my_awesome_feature
```

\5. Every once in a while, merge from updated mainline to stay updated with latest remote changes
```
git checkout mainline
git pull
git checkout my_awesome_feature
git merge mainline
git push
```

\6. Repeat 3, 4, 5 until you are happy with the changes and ready to create a code review. **Commit often**

\7. Update mainline and merge to your branch
```
git checkout mainline
git pull
git checkout my_awesome_feature
git merge mainline
git push
```

\8. (optional) Publish CR
```
cr --parent mainline
```

\9. After CR is approved and you are ready to push your changes to mainline. Switch to mainline and squash merge your commits (from `my_awesome_feature`)
```
git checkout mainline
git pull
git merge --squash my_awesome_feature
git commit
```
Give a nice multiline comment. The first line should summarize the change in less than 80 chars. Leave a blank line after the first line. Now write a novel about your changes.

\10. Check if the commit message and changes are as desired
```
git log
git diff HEAD~1
```

\11. Push!
```
git push
```
